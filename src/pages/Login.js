import { useState, useEffect, useContext } from 'react';
import { Form, Button, Col, Card, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login() {

    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const [isActive, setIsActive] = useState(false);


    function logInUser (e) {
        e.preventDefault()
//---------------
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Biz Shop!"
                })
            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })

            }
        })
//-----------------

        setEmail("");
        setPassword("");
    }
//------------
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        if (email !== '' && password !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }


    }, [email, password])

    return (

        (user.id !== null) ?
        <Navigate to ="/products" />
        :
        <Row className="mt-3 mb-3">
            <Col xs={12} md={6}>
                <Form onSubmit={(e) => logInUser(e)}>
                    
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
        	                type="email" 
        	                placeholder="Enter email" 
        	                value={email}
                            onChange={e => setEmail(e.target.value)}
                            required
                        />
                        <Form.Text className="text-muted">
                            Your email is safe with us.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
        	                type="password" 
        	                placeholder="Password" 
                            value={password}
                            onChange={e => setPassword(e.target.value)}
        	                required
                        />
                    </Form.Group>


                        { isActive ?
                        <Button variant="secondary" type="submit" id="submitBtn">
                            Login
                        </Button>
                        :
                        <Button variant="secondary" type="submit" id="submitBtn" disabled>
                            Login
                        </Button>
                         }


                </Form>

                </Col>
                <Col xs={12} md={6}>
                    <Card className="cardHighlight p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Buy</h2>
                            </Card.Title>
                            <Card.Text>
                                Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            
        </Row>

    )


}