import { useState, useEffect,useContext } from 'react'; 

import {Navigate} from 'react-router-dom'; 
import {useNavigate} from 'react-router-dom'; 

import Swal from 'sweetalert2'; 

import UserContext from '../UserContext'; 

import { Form, Button, Row, Col } from 'react-bootstrap';

export default function Add() {

    const {user} = useContext(UserContext); 

    const navigate = useNavigate(); 

    const [name, setName] = useState(""); 
    const [description, setDescription] = useState(""); 
    const [price, setPrice] = useState("");
    const [quantity, setQuantity] = useState(""); 
    const [isActive, setIsActive] = useState(false);

    function addProduct(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/checkItem`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Duplicate Item Found",
                    icon: "error",
                    text: "Kindly provide new item."
                })
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },

                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                        quantity: quantity
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data) {
                    // Clear input fields
                    setName("");
                    setDescription("");
                    setPrice("");
                    setQuantity("");


                    Swal.fire({
                            title: "Item added!",
                            icon: "success",
                            text: "Happy selling!"
                        })

                        navigate("/products");

                    } else {

                        Swal.fire({
                            title: "Something went wrong",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })
            }
        })

    }

    useEffect(() => {

            if(name !== '' && description !== '' && price >= 1 && quantity >= 1){
                setIsActive(true);
            } else {
                setIsActive(false);
            }

        }, [name, description, price, quantity])


    return (
        (user.id.isAdmin === false) ? // S54 ACTIVITY
        <Navigate to ="/products" /> // S54 ACTIVITY
        : // S54 ACTIVITY
        <Row className="mt-3 mb-3">
            <Col xs={12} md={12}>

                <Form onSubmit={(e) => addProduct(e)}>

                    {/*S55 ACTIVITY*/}
                    <Form.Group className="mb-3" controlId="name">
                    <Form.Label>Item Name</Form.Label>
                    <Form.Control 
                        type="text"
                        value={name}
                        onChange={(e) => {setName(e.target.value)}}
                        placeholder="Enter Item Name" 
                        required
                        />
                  </Form.Group>

                  {/*S55 ACTIVITY*/}
                  <Form.Group className="mb-3" controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                        type="text"
                        value={description}
                        onChange={(e) => {setDescription(e.target.value)}}
                        placeholder="Enter Description" />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                        type="text"
                        value={price}
                        onChange={(e) => {setPrice(e.target.value)}}
                        placeholder="Enter Price" />
                
                  </Form.Group>

                  {/*S55 ACTIVITY*/}
                  <Form.Group className="mb-3" controlId="quantity">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control 
                        type="text"
                        value={quantity}
                        onChange={(e) => {setQuantity(e.target.value)}}
                        placeholder="Enter Quantity" />
                  </Form.Group>

                  { isActive ?
                            <Button variant="secondary" type="submit" id="submitBtn">
                             Add
                            </Button>
                            :
                            <Button variant="secondary" type="submit" id="submitBtn" disabled>
                              Add
                            </Button>
                  }
                 
                </Form> 

                </Col>
            
        </Row>
    )

}