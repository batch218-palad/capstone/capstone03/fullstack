import ProductTable from '../components/ProductTable';

import {useState, useEffect} from 'react';

export default function AllProducts() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductTable key={product._id} product={product} />
				)
			}))
		})
	}, [])

	return(
		<>
		{products}
		</>
	)
}



/*{
						headers: {
							'Content-Type': 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						}
					}*/