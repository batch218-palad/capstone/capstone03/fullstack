import {useState, useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import UserContext from '../UserContext';
import Image from "react-bootstrap/Image";


export default function AppNavbar() {


const { user } = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">

        <Navbar.Brand as={Link} to="/"><img as={NavLink} to="/" class="img-fluid  ml-lg-5 ml-md-4 mr-md-3 rounded-circle" width="25" src="https://i.ibb.co/4t3kwSn/ShopLogo.png"/></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">

            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Catalog</Nav.Link>

            { 
              (user.isAdmin === true && user.id !== null) &&
              <>
                <Nav.Link as={NavLink} to="/create">Add</Nav.Link>
                <Nav.Link as={NavLink} to="/inventory">Inventory</Nav.Link>
                <Nav.Link as={NavLink} to="/users">Users</Nav.Link>
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
               </>
            }

            { 
              (user.isAdmin === false && user.id !== null) &&
              <>
                <Nav.Link as={NavLink} to="/cart">Cart {user.cartTotal}</Nav.Link>
                <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              </>
            }

            { 
              (user.id === null) &&
              <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            }


          </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}