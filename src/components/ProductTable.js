import {useState, useEffect} from 'react';
import { Button, Table } from 'react-bootstrap';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom';

export default function ProductTable({product}) {

  const {name, description, price, quantity, isActive, _id} = product;

  function ProductTable() {
    }

  return (
  <Table striped>
        <thead>
          <tr>
            
            <th>Item Name</th>
            <th>Description</th>
            <th>Price (PhP)</th>
            <th>Quantity Available</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            
            <td>{name}</td>
            <td>{description}</td>
            <td>{price}</td>
            <td>{quantity}</td>
            <td>
              <Button className="bg-primary btn btn-primary" as={Link} to={`/products/${_id}/update`} >Update</Button>
              
              {
              (product.isActive === true) &&
              <Button className="bg-warning btn btn-warning" as={Link} to={`/products/${_id}/archive`} >Archive</Button>
              }
              {
              (product.isActive === false) &&
              <Button className="bg-success btn btn-success" as={Link} to={`/products/${_id}/activate`} >Launch</Button>
              }
              

            </td>
          </tr>

        </tbody>
      </Table>
    );
  }


ProductTable.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired
  })
}