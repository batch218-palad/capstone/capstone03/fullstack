import { useState, useEffect } from "react";

import { UserProvider } from './UserContext';

import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
// import Cart from './pages/ProductView';
import Order from './components/ProductView';
import Users from './pages/User';
import Inventory from './pages/Admin';
import Add from './pages/AddProduct';
import Update from './pages/UpdateProduct';
import Archive from './pages/ArchiveProduct';
import Activate from './pages/ActivateProduct';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import {Container} from 'react-bootstrap';

import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
          //user is logged in
          if(typeof data._id !== "undefined") {

              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          } 
          // user is not logged in
          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }

      })
  }, []);

    return (
      <>
      <UserProvider value={{user, setUser, unsetUser}}>
      {/*Initializes the dynamic routing*/}
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
           <Route path="/" element={<Home />}  />
            <Route path="/products" element={<Products />} />
            <Route path="/products/:productId" element={<ProductView />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            {/*<Route path='/cart' element={<Cart />}/>*/}
            <Route path='/order' element={<Order />}/>
            <Route path='/users' element={<Users />}/>
            <Route path='/inventory' element={<Inventory />}/>
            <Route path='/create' element={<Add />}/>
            <Route path='/products/:productId/update' element={<Update />}/>
            <Route path='/products/:productId/archive' element={<Archive />}/>
            <Route path='/products/:productId/activate' element={<Activate />}/>
            <Route path='/*' element={<Error />}/>
          </Routes>
        </Container>
      </Router>
      </UserProvider>
      </>
    );
  }

  export default App;